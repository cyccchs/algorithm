#include <iostream>
#include <vector>

using namespace std;

class node
{
	public:
		int data = 0;
		int index = 0;
		bool pick = false;
		node* left, * right;
};

node* createNode(int data, int index)
{
	node* newNode = new(node);
	
	newNode->data = data;
	newNode->index = index;
	newNode->left = NULL;
	newNode->right = NULL;

	return newNode;
}

node* createTree(vector<int> a, node* root, int i)
{
	if(i<a.size()){
		node* temp = createNode(a[i], i);
		root = temp;
		root->left = createTree(a, root->left, 2*i+1);
		root->right = createTree(a, root->right, 2*i+2);
	}
	return root;
}

int maxSum(node* root)
{
	if(root->left !=  NULL && root->right != NULL){
		int l = maxSum(root->left);
		int r = maxSum(root->right);
		if(l > r){
			root->left->pick = true;
			return l + root->data;
		}
		else{
			root->right->pick = true;
			return r + root->data;
		}
	}
	else
		return root->data;
}
void findPath(node* root)
{
	if(root->left !=  NULL && root->right != NULL){
		if(root->left->pick){
			cout << root->left->index << " ";
			findPath(root->left);
		}
		else{
			cout << root->right->index << " ";
			findPath(root->right);
		}
	}
}

int main(int argc, char **argv){
	vector<int> weight = {0,2,10,5,6,3,7,99,8,7,6,30,20,2,3};
	node* root = createTree(weight, root, 0);
	cout << "Maximum vertices weight's sum : " << maxSum(root) << endl;
	cout << "Path : 0 ";
	findPath(root);
	cout << endl;
	
	return 0;
}

/*
數據是根據課程投影片設定，indexing 的順序為由上至下由左至右
例如:
index:  0 1 3 7
weight: 0 2 5 99
*/
